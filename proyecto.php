<!DOCTYPE html>
<html lang="es">

<?php
    require("views/layouts/head.html");
?>

<body>
    <?php 
        require("views/layouts/nav.html");
    ?>

    <div class="container bg-light">
        <div class="row">
            <h1 class="col-12 mt-4 mb-4">Proyecto actual: <span class="axelis">Snake</span></h1>
        </div>
        
        <hr class="col-12 mt-3 mb-3">

        <div class="row align-items-center pb-3">
            <div class="col-12 col-sm-4 ps-3 pe-3">
                <img class="rounded w-100" src="views/assets/img/proyecto-example.png" alt="Proyecto actual">
            </div>
            <div class="col-12 col-sm-8 p-3">
                <h4 class="juego">El juego Snake 3:</h4>
                <p class="">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Est possimus consequatur saepe, assumenda sapiente exercitationem beatae odit vero velit quibusdam sequi porro eveniet optio. Ullam tempora modi illo laborum explicabo?</p>
            </div>
        </div>
    </div>
</body>