<!DOCTYPE html>
<html lang="es">

<?php
    require("views/layouts/head.html");
?>

<body>
    <?php 
        require("views/layouts/nav.html");
    ?>

    <div class="container bg-light">
        <div class="row">
            <h1 class="col-12 mt-4 mb-4">Aplicaciones</span></h1>
        </div>
        
        <hr class="col-12 mt-3 mb-3">

        <div class="row card m-3 p-3">
            <div class="col-12 row m-0 p-0">
                <div class="col-12 col-sm-3 m-0 p-0 d-flex align-items-center justify-content-center">
                    <img class="img-thumbnail" src="views/assets/img/Coca-cola.PNG" alt="Coca-cola Vacational Management">
                </div>

                <div class="col-12 col-sm-9 row align-items-center p-4">
                    <h4 class="col-12 aplicacion">Coca-Cola Vacational Management</h4>
                    <p class="col-12">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente qui aut praesentium totam deleniti accusantium delectus voluptate. Tenetur enim tempora eligendi eum sit earum maiores, saepe facere quae. Error, recusandae.</p>
                    <div class="row">
                        <div class="row col">
                            <div class="d-flex col-6 col-lg-auto likes">
                                <a href="#"><img src="views/assets/img/like.png" alt="like"></a>
                                <p>###</p>
                            </div>
        
                            <div class="d-flex col-6 col-lg-auto dislikes">
                                <a href="#"><img src="views/assets/img/dislike.png" alt="dislike"></a>
                                <p>##</p>
                            </div>
                        </div>
    
                        <p class="col-12 col-lg-auto fecha">Publicado: ##/##/#### por Jokerpowered</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row card m-3 p-3">
            <div class="col-12 row m-0 p-0">
                <div class="col-12 col-sm-3 m-0 p-0 d-flex align-items-center justify-content-center">
                    <img class="img-thumbnail" src="views/assets/img/calculadora.png" alt="Coca-cola Vacational Management">
                </div>

                <div class="col-12 col-sm-9 row align-items-center p-4">
                    <h4 class="col-12 aplicacion">Calculadora sencilla</h4>
                    <p class="col-12">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente qui aut praesentium totam deleniti accusantium delectus voluptate. Tenetur enim tempora eligendi eum sit earum maiores, saepe facere quae. Error, recusandae.</p>
                    <div class="row">
                        <div class="row col">
                            <div class="d-flex col-6 col-lg-auto likes">
                                <a href="#"><img src="views/assets/img/like.png" alt="like"></a>
                                <p>###</p>
                            </div>
        
                            <div class="d-flex col-6 col-lg-auto dislikes">
                                <a href="#"><img src="views/assets/img/dislike.png" alt="dislike"></a>
                                <p>##</p>
                            </div>
                        </div>
    
                        <p class="col-12 col-lg-auto fecha">Publicado: ##/##/#### por Jokerpowered</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>