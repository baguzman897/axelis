<!DOCTYPE html>
<html lang="es">

<?php
    require("views/layouts/head.html");
?>

<body>
    <?php 
        require("views/layouts/nav.html");
    ?>

    <div class="container bg-light">
        <div class="row">
            <h1 class="col-12 mt-5 mb-5">¡Bienvenido a <span class="axelis">Axelis</span>!</h1>
        </div>
        
        <div id="panel" class="row align-items-center">
            <h2 class="col-12">Somos un equipo de programación que desarrolla aplicaciones, juegos. ¡Lo que necesites!</h2>
        </div>
    </div>

</body>

</html>