<!DOCTYPE html>
<html lang="es">

<?php
    require("views/layouts/head.html");
?>

<body>
    <?php 
        require("views/layouts/nav.html");
    ?>

    <div class="container bg-light">
        <div class="row">
            <h1 class="col-12 mt-4 mb-4">¡Contáctanos!</span></h1>
        </div>
        
        <hr class="col-12 mt-3 mb-3">

        <div class="row">
            <div class="card col-12 col-xl-6 p-3 sinborde">
                <div class="card-body">
                    <h3 class="mt-3 mb-3">Contacta con nosotros directamente:</h3>
                    <form>
                        <div class="mb-3">
                            <label for="exampleInputName1" class="form-label">Nombre completo:</label>
                            <input type="text" class="form-control" id="exampleInputName1">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            <div id="emailHelp" class="form-text">Nunca compartiremos tú correo con terceros.</div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Nombre completo:</label>
                            <textarea class="form-control"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
    
            <div class="card col-12 col-xl-6 p-3 sinborde">
                <div class="card-body">
                    <div class="row">
                        <h3 class="col-12 mt-3 mb-3">¿Prefieres otros métodos?</h3>
                        <p class="col-12"><b>Correo: </b>soporte@axelis.com</p>
                        <p class="col-12"><b>Teléfono: </b>### ## ##</p>
                        <p class="col-12"><b>Móvil: </b>### ### ## ##</p>
                    </div>
                </div>
            </div>
        </div>

</body>

</html>